package orfiel.dao;

import orfiel.model.Message;

public class FullFormat implements ContentFormatterStrategy{

    @Override
    public String format(Message message) {

        String full = message.getTitle() + " "
                + message.getAuthor() + " "
                + message.getContents() + " "
                + message.getDate();
        return full;
    }


}
