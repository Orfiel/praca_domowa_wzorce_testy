package orfiel.dao;

import orfiel.model.Message;

import java.util.ArrayList;
import java.util.List;

public class SimpleStorage implements Storage {

    private List<Message> storage = new ArrayList<Message>();

    @Override
    public void add(Message message) {
        if (message == null){
            throw new RuntimeException();
        }
        storage.add(message);
    }

    @Override
    public void deleteMessage(Message message) {
        storage.remove(message);
    }

    @Override
    public List<Message> getAll() {
        final ArrayList<Message> messages = new ArrayList<>();
        messages.addAll(storage);
        return messages;
    }
}
