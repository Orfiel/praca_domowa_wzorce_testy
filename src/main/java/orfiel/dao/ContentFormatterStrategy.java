package orfiel.dao;

import orfiel.model.Message;

public interface ContentFormatterStrategy {

    String format(Message message);

}
