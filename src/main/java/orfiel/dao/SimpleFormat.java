package orfiel.dao;

import orfiel.model.Message;

public class SimpleFormat implements ContentFormatterStrategy {



    @Override
    public String format(Message message) {

        String simple =  message.getTitle() + " " + message.getAuthor();
        return simple;
    }
}
