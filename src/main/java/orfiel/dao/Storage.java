package orfiel.dao;

import orfiel.model.Message;

import java.util.List;

public interface Storage {

    void add(Message message);

    void deleteMessage(Message message);

    List<Message> getAll();
}
