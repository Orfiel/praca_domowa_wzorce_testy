package orfiel;

import orfiel.dao.*;
import orfiel.model.Message;
import orfiel.model.MessageBuilder;
import orfiel.service.FiltrServis;
import orfiel.service.MessageService;
import orfiel.view.AddMessage;
import orfiel.view.MessageProvider;
import orfiel.view.ShowMessage;

import java.util.*;

import static orfiel.view.FilterMessage.printDelete;
import static orfiel.view.FilterMessage.printOptions;


public class Main {

    public static void main(String[] args) {

        final MessageProvider messageProvider = new MessageProvider();
        final SimpleStorage simpleStorage = new SimpleStorage();
        final MessageService messageService = new MessageService(simpleStorage);


        Scanner scanner = new Scanner(System.in);

        Message message = new MessageBuilder()
                .withAuthor("Adam")
                .withTitle("Początek")
                .withContents("jakiś tekst")
                .build();

        Message message1 = new MessageBuilder()
                .withAuthor("Piotr")
                .withTitle("Początek")
                .withContents("jakiś tekst ale inny")
                .build();

        Message message2 = new MessageBuilder()
                .withAuthor("wacek")
                .withTitle("Początek i koniec")
                .withContents("jakiś tekstjeszcze inny")
                .build();


        messageService.addMessage(message);
        messageService.addMessage(message1);
        messageService.addMessage(message2);

        mainLoop:
        while (true) {
            System.out.println();
            System.out.println("Co chcesz zrobić");
            System.out.println("1: Dodaj wiadomość");
            System.out.println("2: Usuń wiadomość");
            System.out.println("3: Wyświetl wszystkie wiadomości");
            System.out.println("4: Filtruj wiadomości");
            System.out.println("5: Zamknji aplikacje");
            System.out.println();

            int userAction = scanner.nextInt();

            switch (userAction) {
                case 1: {
                    new AddMessage(messageProvider, messageService).addBuildedMessage();
                    break;
                }
                case 2: {
                    System.out.println("Wybierz pierwszy filtr:");
                    printOptions();

                    List<Message> messagesNextFiltr = messageService.getAllMessage();
                    boolean running = true;
                    filterLoop:
                    while (running) {

                        System.out.println("Podaj po czym chcesz filtrować: ");
                        int filterType = scanner.nextInt();
                        scanner.nextLine();

                        System.out.println("Wpisz predykat filtra: ");
                        String filterValue = scanner.nextLine();

                        messagesNextFiltr = new FiltrServis(messageService)
                                .predicatFiltred(filterValue, filterType, messagesNextFiltr);


                        System.out.println(messagesNextFiltr);
                        printDelete();
                        String decide = scanner.nextLine();

                        if (decide == "y"){
                            messagesNextFiltr = new MessageService().deleteMessage(messagesNextFiltr);
                            break;
                        }

                        printOptions();
                        userAction = scanner.nextInt();
                        if (userAction == 5) {
                            break filterLoop;
                        }
                    }
                    break;

                }
                case 3: {
                    new ShowMessage(messageService).setFormat();
                    new ShowMessage(messageService).print();
                    break;
                }
                case 4: {
                    System.out.println("Wybierz pierwszy filtr:");
                    printOptions();

                    List<Message> messagesNextFiltr = messageService.getAllMessage();
                    boolean running = true;
                    filterLoop:
                    while (running) {

                        System.out.println("Podaj co chcesz filtrować:");
                        int filterType = scanner.nextInt();
                        scanner.nextLine();

                        System.out.println("Wpisz wartość filtra:");
                        String filterValue = scanner.nextLine();

                        messagesNextFiltr = new FiltrServis(messageService)
                                .predicatFiltred(filterValue, filterType, messagesNextFiltr);


                        System.out.println(messagesNextFiltr);
                        printOptions();
                        userAction = scanner.nextInt();
                        if (userAction == 5) {
                            break filterLoop;
                        }

                    }
                    break;
                }
                case 5: {
                    scanner.close();
                    break mainLoop;
                }

            }

        }
    }


}

