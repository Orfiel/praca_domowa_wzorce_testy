package orfiel.model;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;

public class Message {

    private String title;
    private String author;
    private String contents;
    private Date date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message = (Message) o;
        return Objects.equals(title, message.title) &&
                Objects.equals(author, message.author) &&
                Objects.equals(contents, message.contents) &&
                Objects.equals(date, message.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, contents, date);
    }

    @Override
    public String toString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", contents='" + contents + '\'' +
                ", date=" + date +
                '}';
    }
}
