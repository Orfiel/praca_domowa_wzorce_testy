package orfiel.model;


import java.util.Date;

public class MessageBuilder {
    String title;
    String author;
    String contents;
    Date date = new Date();

    public MessageBuilder withTitle(String title){
        this.title = title;
        return this;
    }
    public MessageBuilder withAuthor(String author){
        this.author = author;
        return this;
    }
    public MessageBuilder withContents(String contents){
        this.contents = contents;
        return this;
    }
    public Message build() {
        Message message = new Message();
        message.setTitle(this.title);
        message.setAuthor(this.author);
        message.setContents(this.contents);
        message.setDate(this.date);
        return message;
    }

}
