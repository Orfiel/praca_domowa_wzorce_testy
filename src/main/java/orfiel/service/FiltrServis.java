package orfiel.service;

import orfiel.model.Message;

import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FiltrServis {

    private MessageService messageService;

    public FiltrServis(MessageService messageService) {
        this.messageService = messageService;
    }

    Scanner scanner = new Scanner(System.in);

    private Predicate<Message> setPredicat(int filtreType, String value) {

        Predicate<Message> predicate = message -> true;

        switch (filtreType) {
            case 1: {
                predicate = message -> message.getTitle().equals(value);
                System.out.println();
                break;
            }
            case 2: {
                predicate = message -> message.getAuthor().equals(value);
                System.out.println();
                break;
            }
            case 3: {
                predicate = message -> message.getContents().equals(value);
                System.out.println();
                break;
            }
            case 4: {
                predicate = message -> message.getContents().equals(value);
                System.out.println();
                break;
            }
            case 5: {
            }
        }
        return predicate;
    }

    public List<Message> predicatFiltred(String filterValue, int filterType, List<Message> messages) {

        Predicate<Message> predicate = setPredicat(filterType, filterValue);

        List<Message> result = messages.stream()
                .filter(predicate)
                .collect(Collectors.toList());
        return result;
    }


}
