package orfiel.service;

import com.sun.corba.se.pept.protocol.MessageMediator;
import orfiel.dao.*;
import orfiel.model.Message;

import java.util.List;

public class MessageService {
    private Storage storage;
    private ContentFormatterStrategy strategy;

    public ContentFormatterStrategy getStrategy() {
        return strategy;
    }

    public MessageService() {
        strategy = new FullFormat();
    }
    public void set(ContentFormatterStrategy strategy) {
        this.strategy = strategy;
    }
    public String print(Message message){
        String formattedMessage = strategy.format(message);
        return formattedMessage;
    }
    public MessageService( Storage storage) {
        this.storage = storage;
    }
    public void addMessage(Message message) {
        storage.add(message);
    }

    public List<Message> getAllMessage() {
        return storage.getAll();
    }
    public List<Message> deleteMessage(List<Message> messages){
        messages.stream().forEach(message -> storage.deleteMessage(message));
        return messages;
    }
}