package orfiel.view;

import orfiel.model.Message;
import orfiel.model.MessageBuilder;

import java.util.Optional;
import java.util.Scanner;

public class MessageProvider {

    private final Scanner scanner;

    private Optional<String> getTitleFtomUser(){
        System.out.println("Wprowadź tytuł:");
        return Optional.of(scanner.nextLine());
    }
    private Optional<String> geAuthorFtomUser(){
        System.out.println("Wprowadź autora:");
        return Optional.of(scanner.nextLine());
    }
    private Optional<String> getContentFtomUser(){
        System.out.println("Wprowadź treść wiadomości:");
        return Optional.of(scanner.nextLine());
    }

    public MessageProvider() {
        scanner = new Scanner(System.in);
    }
    public Message messageBuild(){
        MessageBuilder messageBuilder = new MessageBuilder();
        getTitleFtomUser().map(messageBuilder::withTitle);
        geAuthorFtomUser().map(messageBuilder::withAuthor);
        getContentFtomUser().map(messageBuilder::withContents);

        return messageBuilder.build();
    }







}
