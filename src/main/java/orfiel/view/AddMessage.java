package orfiel.view;

import orfiel.model.Message;
import orfiel.service.MessageService;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.Scanner;

public class AddMessage {

    final MessageProvider messageProvider;
    final MessageService messageService;

    public AddMessage(MessageProvider messageProvider, MessageService messageService) {
        this.messageProvider = messageProvider;
        this.messageService = messageService;
    }

    public void addBuildedMessage() {
        final Message message = messageProvider.messageBuild();
        messageService.addMessage(message);
    }
}
