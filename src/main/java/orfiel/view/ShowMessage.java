package orfiel.view;

import orfiel.dao.ContentFormatterStrategy;
import orfiel.dao.FullFormat;
import orfiel.dao.SimpleFormat;
import orfiel.dao.Storage;
import orfiel.model.Message;
import orfiel.service.MessageService;
import java.util.Scanner;

public class ShowMessage {
    Scanner scanner = new Scanner(System.in);
    private ContentFormatterStrategy strategy;
    private Storage storage;
    MessageService messageService;

    public ShowMessage(MessageService messageService) {
        this.messageService = messageService;
    }

    public void setFormat() {
        System.out.println("Wybierz sposób wyświetlania:");
        System.out.println("1: Tytuł; Autor;");
        System.out.println("2: Tytuł; Author; Treść; Data;");
        int viewType = scanner.nextInt();


        switch (viewType) {
            case 1: {
                new SimpleFormat();
                messageService.set(new SimpleFormat());
                System.out.println("Simple Format");
                break;
            }
            case 2: {
                messageService.set(new FullFormat());
                System.out.println("Full Format");
                break;
            }
        }
    }

    public void print() {
        for (Message message : messageService.getAllMessage()) {
            strategy = messageService.getStrategy();
            System.out.println(strategy.format(message));
        }

    }
}
