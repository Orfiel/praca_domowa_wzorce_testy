package orfiel.dao;

import orfiel.model.Message;
import orfiel.model.MessageBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest(SimpleStorage.class)
public class SimpleStorageTest {

    SimpleStorage menager;
    Message message1, message2;

    @Mock
    private Storage storage;
    @Mock
    private MessageBuilder builder;

@Before
public void create_message() {
    menager = new SimpleStorage();
    builder = new MessageBuilder();

    message1 = builder.withTitle("TitledBorder").withAuthor("Author1").withContents("Conetent1").build();
    message2 = builder.withTitle("TitledBorder2").withAuthor("Author2").withContents("Conetent2").build();


}
@Test (expected = RuntimeException.class)
public void throw_expection_if_null() throws RuntimeException {
    menager.add(null);}

@Test
    public void adding_to_list() {

    menager.add(message1);
    menager.add(message2);
    Assertions.assertThat(menager.getAll()).contains(message1);
    Assertions.assertThat(menager.getAll()).contains(message2);

}}