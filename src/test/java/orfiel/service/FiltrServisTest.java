package orfiel.service;

import orfiel.dao.SimpleStorage;
import orfiel.dao.Storage;
import orfiel.model.Message;
import orfiel.model.MessageBuilder;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
@PrepareForTest(FiltrServis.class)
public class FiltrServisTest {

    SimpleStorage simpleStorage = new SimpleStorage();

    @Mock
    private Storage storage;

    private MessageBuilder builder = new MessageBuilder();
    @Mock
    MessageService messageService;


    @Test
    public void check_author_predicate() {
        FiltrServis filtrServis = new FiltrServis(messageService);
        Message message1, message2, message3, message4, message5, message6;
        message1 = builder.withTitle("Title1").withAuthor("Author1").withContents("Conetent1").build();
        message2 = builder.withTitle("Title2").withAuthor("Author2").withContents("Conetent2").build();
        message3 = builder.withTitle("Title3").withAuthor("Author3").withContents("Contents3").build();
        message4 = builder.withTitle("Title4").withAuthor("Author4").withContents("Conetent4").build();
        message5 = builder.withTitle("Title5").withAuthor("Author5").withContents("Conetent5").build();
        message6 = builder.withTitle("Title6").withAuthor("Author6").withContents("Contents6").build();

        List<Message> testowa = new ArrayList();
        testowa.add(message1);
        testowa.add(message2);
        testowa.add(message3);
        testowa.add(message4);
        testowa.add(message5);
        testowa.add(message6);

        List<Message> filtredList = filtrServis.predicatFiltred("Author5", 2, testowa);

        Assertions.assertThat(filtredList.size()).isEqualTo(1);
    }

    @Test
    public void check_title_predicate() {
        FiltrServis filtrServis = new FiltrServis(messageService);
        Message message1, message2, message3, message4, message5, message6;
        message1 = builder.withTitle("Title1").withAuthor("Author1").withContents("Conetent1").build();
        message2 = builder.withTitle("Title2").withAuthor("Author2").withContents("Conetent2").build();
        message3 = builder.withTitle("Title3").withAuthor("Author3").withContents("Contents3").build();
        message4 = builder.withTitle("Title4").withAuthor("Author4").withContents("Conetent4").build();
        message5 = builder.withTitle("Title5").withAuthor("Author5").withContents("Conetent5").build();
        message6 = builder.withTitle("Title6").withAuthor("Author6").withContents("Contents6").build();

        List<Message> testowa = new ArrayList();
        testowa.add(message1);
        testowa.add(message2);
        testowa.add(message3);
        testowa.add(message4);
        testowa.add(message5);
        testowa.add(message6);

        List<Message> filtredList = filtrServis.predicatFiltred("Title2", 1, testowa);

        Assertions.assertThat(filtredList.size()).isEqualTo(1);
    }

    @Test
    public void check_content_predicate() {
        FiltrServis filtrServis = new FiltrServis(messageService);
        Message message1, message2, message3, message4, message5, message6;
        message1 = builder.withTitle("Title1").withAuthor("Author1").withContents("Conetent1").build();
        message2 = builder.withTitle("Title2").withAuthor("Author2").withContents("Conetent2").build();
        message3 = builder.withTitle("Title3").withAuthor("Author3").withContents("Contents3").build();
        message4 = builder.withTitle("Title4").withAuthor("Author4").withContents("Conetent4").build();
        message5 = builder.withTitle("Title5").withAuthor("Author5").withContents("Conetent5").build();
        message6 = builder.withTitle("Title6").withAuthor("Author6").withContents("Contents6").build();

        List<Message> testowa = new ArrayList();
        testowa.add(message1);
        testowa.add(message2);
        testowa.add(message3);
        testowa.add(message4);
        testowa.add(message5);
        testowa.add(message6);

        List<Message> filtredList = filtrServis.predicatFiltred("Contents6", 3, testowa);

        Assertions.assertThat(filtredList.size()).isEqualTo(1);
    }

}
